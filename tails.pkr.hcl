variable "cloud_token" {
  type = string
}

variable "hostname" {
  type = string
  default = "tails"
}

variable "ssh_fullname" {
  default = "Vagrant"
}

variable "ssh_username" {
  default = "vagrant"
}

variable "ssh_password" {
  default = "vagrant"
}

locals {
  version = "0.1.{{timestamp}}"
}

source "virtualbox-iso" "basic-example" {
  guest_os_type = "Linux26_64"

  iso_url = "https://mirrors.ukfast.co.uk/sites/tails.boum.org/tails/stable/tails-amd64-4.19/tails-amd64-4.19.iso"
  # iso_checksum = "file:https://tails.boum.org/torrents/files/tails-amd64-4.19.iso.sig"
  iso_checksum = "none"

  boot_command = [
    "<esc><esc><enter><wait>",
    "/install/vmlinuz noapic",
    " initrd=/install/initrd.gz",
    " auto=true",
    " priority=critical",
    " hostname={{user `hostname`}}",
    " passwd/user-fullname={{user `ssh_fullname`}}",
    " passwd/username={{user `ssh_username`}}",
    " passwd/user-password={{user `ssh_password`}}",
    " passwd/user-password-again={{user `ssh_password`}}",
    " preseed/url=http://{{.HTTPIP}}:{{.HTTPPort}}/preseed.cfg",
    " -- <enter>"
  ]

  ssh_username = "vagrant"
  ssh_password = "vagrant"
  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
}

build {
  sources = ["sources.virtualbox-iso.basic-example"]
  # provisioner "shell" {
  #   inline = [ "sudo hostnamectl set-hostname kali.local" ]
  # }
  # provisioner "chef-solo" {
  #   cookbook_paths = ["chef/cookbooks"]
  #   run_list = [
  #     "ckrst::default",
  #     "ckrst::keyboard_conf"
  #   ]
  # }
  provisioner "file" {
    source = "files/keyboard"
    destination = "/home/vagrant/keyboard"
  }
  provisioner "shell" {
    inline = [
      "sudo mv /home/vagrant/keyboard /etc/default/keyboard",
      "sudo chown root:root /etc/default/keyboard"
    ]
  }
  post-processor "vagrant-cloud" {
    access_token = var.cloud_token
    box_tag = "vinik/tails"
    version = "${local.version}"
  }
}
