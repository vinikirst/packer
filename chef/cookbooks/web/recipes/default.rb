#
# Cookbook:: web
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

service 'apache2' do
  extend Apache2::Cookbook::Helpers
  service_name lazy { apache_platform_service_name }
  supports restart: true, status: true, reload: true
  action :nothing
end

apache2_install 'default_install'

apache2_module 'ssl'
apache2_module 'headers'
apache2_module 'rewrite'
apache2_mod_php 'php'

mysql_service 'db' do
  port '3306'
  version '5.7'
  initial_root_password 'ckrst@dm1n'
end

mysql_client 'default' do
  action :create
end


apt_repository 'certbot' do
  uri 'ppa:certbot/certbot'
end

package 'python-certbot-apache'
