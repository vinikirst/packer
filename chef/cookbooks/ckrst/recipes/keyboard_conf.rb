#
# Cookbook:: ckrst
# Recipe:: keyboard_conf
#
# Copyright:: 2021, The Authors, All Rights Reserved.

file "/etc/default/keyboard" do
  content '# KEYBOARD CONFIGURATION FILE

# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="br"
XKBVARIANT=""
XKBOPTIONS=""

BACKSPACE="guess"
'
  mode '0644'
  owner 'root'
  group 'root'
end
