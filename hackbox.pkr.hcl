variable "cloud_token" {
  type = string
}

locals {
  version = "0.1.{{timestamp}}"
}

source "vagrant" "hackbox-kali" {
  communicator = "ssh"
  source_path = "kalilinux/rolling"
  provider = "virtualbox"
  box_name = "vinik/hackbox-kali"
  add_force = true
}

source "docker" "hackbox-kali" {
    image = "kalilinux/kali-rolling"
    export_path = "image.tar"
}

build {
  sources = ["source.docker.hackbox-kali"]
  provisioner "shell" {
    inline = [
      "apt-get update",
      "apt-get upgrade -y",
      "apt-get dist-upgrade -y",
      "apt-get install -y exploitdb exploitdb-bin-sploits git gdb gobuster hashcat hydra man-db minicom nasm nmap python3 pip sqlmap sslscan wordlists",
      "mkdir /root/.ssh",
      "touch /root/.ssh/known_hosts",
      "ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts",
      "ssh-keyscan github.com >> /root/.ssh/known_hosts",
      "git clone https://github.com/danielmiessler/SecLists.git /opt/seclists",
      "git clone https://github.com/PowerShellMafia/PowerSploit.git /opt/powersploit",
      "pip install pwntools",
      "PATH=$PATH:/opt/powersploit"
    ]
  }

  post-processors {
    post-processor "docker-import" {
      repository =  "vinik/hackbox"
      tag = "0.1"
      # tag = "latest"
    }
    post-processor "docker-push" {}
  }


}


build {
  sources = ["sources.vagrant.hackbox-kali"]

  provisioner "file" {
    source = "files/keyboard"
    destination = "/home/vagrant/keyboard"
    only = ["vagrant.hackbox-kali"]
  }
  # teclado br
  provisioner "shell" {
    inline = [
      "sudo mv /home/vagrant/keyboard /etc/default/keyboard",
      "sudo chown root:root /etc/default/keyboard"
    ]
    only = ["vagrant.hackbox-kali"]
  }

  # poor mans pentest
  provisioner "shell" {
    inline = [
      "cd /home/vagrant",
      "git clone https://github.com/JohnHammond/poor-mans-pentest.git"
    ]
    only = ["vagrant.hackbox-kali"]
  }

  # wappalyzer
  provisioner "shell" {
    inline = [
      "sudo apt install -y npm",
      "sudo npm i -g wappalyzer"
    ]
    only = ["vagrant.hackbox-kali"]
  }

  post-processor "vagrant-cloud" {
    access_token = var.cloud_token
    box_tag = "vinik/hackbox-kali"
    version = "${local.version}"
    only = ["vagrant.hackbox-kali"]
  }
}
